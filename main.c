#include "main.h"
#include "driverlib/driverlib.h"
#include "hal_LCD.h"
#include <msp430.h> 
#include <stdio.h>

#define MOISTURE 0;
#define TEMP 1;
#define BACK 2;


void delay_us(unsigned int us);

char sensor_sel_text[3][6] = {
    {"MOIS  "}, {"TEMP  "}, {"BACK  "}
};
int sensorLimits[3][2];

typedef enum {ZONE_SEL, SENSOR_SEL, SENSOR_CTR} MODE;



/*
 * This project contains some code samples that may be useful.
 *
 * UART: It configures P1.0 and P1.1 to be connected internally to the
 * eSCSI module, which is a serial communications module, and places it
 * in UART mode. This let's you communicate with the PC via a software
 * COM port over the USB cable. You can use a console program, like PuTTY,
 * to type to your LaunchPad. The code in this sample just echos back
 * whatever character was received.
 *
 * ADC:
 *
 * PWM:
 */

char ADCState = 0; //Busy state of the ADC
int16_t ADCResult = 0; //Storage for the ADC conversion result

void main(void)
{
    MODE currentMode = ZONE_SEL;
    int currentZone = 0;
    int currentSensor = 0;
    int i;
    int j;
    for(i = 0; i < 3; i++) {
        for(j = 0; j < 2; j++) {
            sensorLimits[i][j] = 800;
        }
    }
    
    char lcd_text[6];
    
    int buttonState1 = 0;
    int buttonState2 = 0;
    int pb1;
    int pb2;
    volatile int sensorValue;
    int timeoutCounter;


    /*
     * Functions with two underscores in front are called compiler intrinsics.
     * They are documented in the compiler user guide, not the IDE or MCU guides.
     * They are a shortcut to insert some assembly code that is not really
     * expressible in plain C/C++. Google "MSP430 Optimizing C/C++ Compiler
     * v18.12.0.LTS" and search for the word "intrinsic" if you want to know
     * more.
     * */

    //Turn off interrupts during initialization
    __disable_interrupt();

    //Stop watchdog timer unless you plan on using it
    WDT_A_hold(WDT_A_BASE);

    // Initializations - see functions for more detail
    Init_GPIO();    //Sets all pins to output low as a default
    Init_ADC();     //Sets up the ADC to sample
    Init_Clock();   //Sets up the necessary system clocks
    Init_UART();    //Sets up an echo over a COM port
    Init_LCD();     //Sets up the LaunchPad LCD display

     /*
     * The MSP430 MCUs have a variety of low power modes. They can be almost
     * completely off and turn back on only when an interrupt occurs. You can
     * look up the power modes in the Family User Guide under the Power Management
     * Module (PMM) section. You can see the available API calls in the DriverLib
     * user guide, or see "pmm.h" in the driverlib directory. Unless you
     * purposefully want to play with the power modes, just leave this command in.
     */
    PMM_unlockLPM5(); //Disable the GPIO power-on default high-impedance mode to activate previously configured port settings

    //All done initializations - turn interrupts back on.
    __enable_interrupt();
    
    timeoutCounter = 0;
    
    while(1) {
        pb1 = GPIO_getInputPinValue(SW1_PORT, SW1_PIN);
        pb2 = GPIO_getInputPinValue(SW2_PORT, SW2_PIN);
        
        switch(currentMode) {
            case ZONE_SEL:
                sprintf(lcd_text, "ZONE %d", currentZone + 1);
                displayText(lcd_text);
                
                if (pb1 & !buttonState1) {
                    Timer_A_stop(TIMER_A0_BASE);    //Shut off PWM signal
                    buttonState1 = 1;                //Capture new button state
                } else if (!pb1 & buttonState1) {
                    Timer_A_outputPWM(TIMER_A0_BASE, &param);   //Turn on PWM
                    currentZone = (currentZone + 1) % 3;
                    buttonState1 = 0;                            //Capture new button state
                }  
                
                if (pb2 & !buttonState2) {
                    Timer_A_stop(TIMER_A0_BASE);    //Shut off PWM signal
                    buttonState2 = 1;                //Capture new button state
                } else if (!pb2 & buttonState2) {
                    Timer_A_outputPWM(TIMER_A0_BASE, &param);   //Turn on PWM
                    currentMode = SENSOR_SEL;
                    buttonState2 = 0;                            //Capture new button state
                }
                
                break;
            case SENSOR_SEL:
                displayText(sensor_sel_text[currentSensor]);
                
                if (pb1 & !buttonState1) {
                    Timer_A_stop(TIMER_A0_BASE);    //Shut off PWM signal
                    buttonState1 = 1;                //Capture new button state
                } else if (!pb1 & buttonState1) {
                    Timer_A_outputPWM(TIMER_A0_BASE, &param);   //Turn on PWM
                    currentSensor = (currentSensor + 1) % 3;
                    buttonState1 = 0;                            //Capture new button state
                }  
                
                if (pb2 & !buttonState2) {
                    Timer_A_stop(TIMER_A0_BASE);    //Shut off PWM signal
                    buttonState2 = 1;                //Capture new button state
                } else if (!pb2 & buttonState2) {
                    Timer_A_outputPWM(TIMER_A0_BASE, &param);   //Turn on PWM
                    if (currentSensor == 2) {
                        currentMode = ZONE_SEL;
                    } else {
                        currentMode = SENSOR_CTR;
                    }
                    buttonState2 = 0;                            //Capture new button state
                }
                
                break;
            case SENSOR_CTR:
                if (currentSensor == 0) {
                    sprintf(lcd_text, "M %d", sensorLimits[currentSensor][currentZone]);
                } else {
                    sprintf(lcd_text, "T %d", sensorLimits[currentSensor][currentZone]);
                }
                displayText(lcd_text);
                timeoutCounter++;
                
                if (pb1 & !buttonState1) {
                    Timer_A_stop(TIMER_A0_BASE);    //Shut off PWM signal
                    buttonState1 = 1;                //Capture new button state
                } else if (!pb1 & buttonState1) {
                    Timer_A_outputPWM(TIMER_A0_BASE, &param);   //Turn on PWM
                    sensorLimits[currentSensor][currentZone] -= 10;
                    timeoutCounter = 0;
                    //sensorLimits[currentZone][currentSensor]--;
                    buttonState1 = 0;                            //Capture new button state
                }  
                
                if (pb2 & !buttonState2) {
                    Timer_A_stop(TIMER_A0_BASE);    //Shut off PWM signal
                    buttonState2 = 1;                //Capture new button state
                } else if (!pb2 & buttonState2) {
                    Timer_A_outputPWM(TIMER_A0_BASE, &param);   //Turn on PWM
                    sensorLimits[currentSensor][currentZone] += 10;
                    timeoutCounter = 0;
                    //sensorLimits[currentZone][currentSensor]++;
                    buttonState2 = 0;                            //Capture new button state
                }
                
                if (timeoutCounter == 20) {
                    Timer_A_stop(TIMER_A0_BASE);
                    currentMode = ZONE_SEL;
                    currentZone = 0;
                    currentSensor = 0;
                    timeoutCounter = 0;
                    buttonState1 = 0;
                    buttonState2 = 0;
                }
                
                break;
        }
        
       
        GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN5); //S0
        GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN2); //S1
        GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN3); //S2
        sensorValue = ADC();
        if (sensorValue > sensorLimits[1][2]) {
            GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN7);
        } else {
            GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN7);
        }
        __delay_cycles(50000);
        
        GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN5); //S0
        GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN2); //S1
        GPIO_setOutputHighOnPin(GPIO_PORT_P8, GPIO_PIN3); //S2
        sensorValue = ADC();
        if (sensorValue > sensorLimits[1][1]) {
            GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN6);
        } else {
            GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN6);
        }
        __delay_cycles(50000);
        
        
        GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN5); //S0
        GPIO_setOutputHighOnPin(GPIO_PORT_P8, GPIO_PIN2); //S1
        GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN3); //S2
        sensorValue = ADC();
        if (sensorValue > sensorLimits[1][0]) {
            GPIO_setOutputHighOnPin(GPIO_PORT_P5, GPIO_PIN2);
        } else {
            GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_PIN2);
        }
        __delay_cycles(50000);
        
        GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN5); //S0
        GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN2); //S1
        GPIO_setOutputHighOnPin(GPIO_PORT_P8, GPIO_PIN3); //S2
        sensorValue = ADC();
        if (sensorValue > sensorLimits[0][2]) {
            GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN3);
        } else {
            GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN3);
        }
        __delay_cycles(50000);
        
        GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN5); //S0
        GPIO_setOutputHighOnPin(GPIO_PORT_P8, GPIO_PIN2); //S1
        GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN3); //S2
        sensorValue = ADC();
        if (sensorValue > sensorLimits[0][1]) {
            GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN4);
        } else {
            GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN4);
        }
        __delay_cycles(50000);
        
        GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN5); //S0
        GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN2); //S1
        GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN3); //S2
        
        sensorValue = ADC();
        if (sensorValue > sensorLimits[0][0]) {
            GPIO_setOutputHighOnPin(GPIO_PORT_P1, GPIO_PIN5);
        } else {
            GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN5);
        }
        __delay_cycles(50000);
    }


    /*
     * You can use the following code if you really do plan on only using interrupts
     * to handle all your system events since you don't need any infinite loop of code.
     *
     * //Enter LPM0 - interrupts only
     * __bis_SR_register(LPM0_bits);
     * //For debugger to let it know that you meant for there to be no more code
     * __no_operation();
    */

}

int ADC() {
    //ADC Configuration Soil Moisture connected to P8.3, Temp to 8.0

        GPIO_setAsPeripheralModuleFunctionInputPin(ADC_IN_PORT, ADC_IN_PIN, GPIO_PRIMARY_MODULE_FUNCTION);

        volatile int ADC_value = 0;
//        SYSCFG2 |= ADCPCTL8;

        ADC_init(ADC_BASE, ADC_SAMPLEHOLDSOURCE_SC, ADC_CLOCKSOURCE_ADCOSC, ADC_CLOCKDIVIDER_1);
        ADC_enable(ADC_BASE);

        ADC_setupSamplingTimer(ADC_BASE, ADC_CYCLEHOLD_16_CYCLES, ADC_MULTIPLESAMPLESDISABLE);
        ADC_configureMemory(ADC_BASE, ADC_INPUT_A8, ADC_VREFPOS_INT, ADC_VREFNEG_AVSS);

         ADC_startConversion(ADC_BASE, ADC_SINGLECHANNEL);

         delay_us(500);
         //__delay_cycles(500);
         while (!(ADC_getInterruptStatus(ADC_BASE, ADC_COMPLETED_INTERRUPT_FLAG)));
         ADC_value = ADC_getResults(ADC_BASE);

         ADC_clearInterrupt(ADC_BASE, ADC_COMPLETED_INTERRUPT_FLAG);
         ADC_disable(ADC_BASE);
         return ADC_value;
}

void delay_us(unsigned int us){
    unsigned int i;
    for (i = 0; i<=us/2; i++){
        _delay_cycles(1);
    }
}

void Init_GPIO(void)
{
    // Set all GPIO pins to output low to prevent floating input and reduce power consumption
    GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P3, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P4, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P5, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P6, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P7, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setOutputLowOnPin(GPIO_PORT_P8, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);

    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P3, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P4, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P6, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P7, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);
    GPIO_setAsOutputPin(GPIO_PORT_P8, GPIO_PIN0|GPIO_PIN1|GPIO_PIN2|GPIO_PIN3|GPIO_PIN4|GPIO_PIN5|GPIO_PIN6|GPIO_PIN7);

    //Set LaunchPad switches as inputs - they are active low, meaning '1' until pressed
    GPIO_setAsInputPinWithPullUpResistor(SW1_PORT, SW1_PIN);
    GPIO_setAsInputPinWithPullUpResistor(SW2_PORT, SW2_PIN);

    //Set LED1 and LED2 as outputs
    //GPIO_setAsOutputPin(LED1_PORT, LED1_PIN); //Comment if using UART
    GPIO_setAsOutputPin(LED2_PORT, LED2_PIN);
}

/* Clock System Initialization */
void Init_Clock(void)
{
    /*
     * The MSP430 has a number of different on-chip clocks. You can read about it in
     * the section of the Family User Guide regarding the Clock System ('cs.h' in the
     * driverlib).
     */

    /*
     * On the LaunchPad, there is a 32.768 kHz crystal oscillator used as a
     * Real Time Clock (RTC). It is a quartz crystal connected to a circuit that
     * resonates it. Since the frequency is a power of two, you can use the signal
     * to drive a counter, and you know that the bits represent binary fractions
     * of one second. You can then have the RTC module throw an interrupt based
     * on a 'real time'. E.g., you could have your system sleep until every
     * 100 ms when it wakes up and checks the status of a sensor. Or, you could
     * sample the ADC once per second.
     */
    //Set P4.1 and P4.2 as Primary Module Function Input, XT_LF
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P4, GPIO_PIN1 + GPIO_PIN2, GPIO_PRIMARY_MODULE_FUNCTION);

    // Set external clock frequency to 32.768 KHz
    CS_setExternalClockSource(32768);
    // Set ACLK = XT1
    CS_initClockSignal(CS_ACLK, CS_XT1CLK_SELECT, CS_CLOCK_DIVIDER_1);
    // Initializes the XT1 crystal oscillator
    CS_turnOnXT1LF(CS_XT1_DRIVE_1);
    // Set SMCLK = DCO with frequency divider of 1
    CS_initClockSignal(CS_SMCLK, CS_DCOCLKDIV_SELECT, CS_CLOCK_DIVIDER_1);
    // Set MCLK = DCO with frequency divider of 1
    CS_initClockSignal(CS_MCLK, CS_DCOCLKDIV_SELECT, CS_CLOCK_DIVIDER_1);
}

/* UART Initialization */
void Init_UART(void)
{
    //Configure UART pins, which maps them to a COM port over the USB cable
    //Set P1.0 and P1.1 as Secondary Module Function Input.
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P1, GPIO_PIN1, GPIO_PRIMARY_MODULE_FUNCTION);
    GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P1, GPIO_PIN0, GPIO_PRIMARY_MODULE_FUNCTION);

    /*
     * UART Configuration Parameter. These are the configuration parameters to
     * make the eUSCI A UART module to operate with a 9600 baud rate. These
     * values were calculated using the online calculator that TI provides at:
     * http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSP430BaudRateConverter/index.html
     */

    //SMCLK = 1MHz, Baudrate = 9600
    //UCBRx = 6, UCBRFx = 8, UCBRSx = 17, UCOS16 = 1
    EUSCI_A_UART_initParam param = {0};
        param.selectClockSource = EUSCI_A_UART_CLOCKSOURCE_SMCLK;
        param.clockPrescalar    = 6;
        param.firstModReg       = 8;
        param.secondModReg      = 17;
        param.parity            = EUSCI_A_UART_NO_PARITY;
        param.msborLsbFirst     = EUSCI_A_UART_LSB_FIRST;
        param.numberofStopBits  = EUSCI_A_UART_ONE_STOP_BIT;
        param.uartMode          = EUSCI_A_UART_MODE;
        param.overSampling      = 1;

    if(STATUS_FAIL == EUSCI_A_UART_init(EUSCI_A0_BASE, &param))
    {
        return;
    }

    EUSCI_A_UART_enable(EUSCI_A0_BASE);

    EUSCI_A_UART_clearInterrupt(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);

    // Enable EUSCI_A0 RX interrupt
    EUSCI_A_UART_enableInterrupt(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT);
}

/* EUSCI A0 UART ISR - Echoes data back to PC host */
#pragma vector=USCI_A0_VECTOR
__interrupt
void EUSCIA0_ISR(void)
{
    uint8_t RxStatus = EUSCI_A_UART_getInterruptStatus(EUSCI_A0_BASE, EUSCI_A_UART_RECEIVE_INTERRUPT_FLAG);

    EUSCI_A_UART_clearInterrupt(EUSCI_A0_BASE, RxStatus);

    if (RxStatus)
    {
        EUSCI_A_UART_transmitData(EUSCI_A0_BASE, EUSCI_A_UART_receiveData(EUSCI_A0_BASE));
    }
}

void Init_ADC(void)
{
    /*
     * To use the ADC, you need to tell a physical pin to be an analog input instead
     * of a GPIO, then you need to tell the ADC to use that analog input. Defined
     * these in main.h for A9 on P8.1.
     */

    //Set ADC_IN to input direction
    GPIO_setAsPeripheralModuleFunctionInputPin(ADC_IN_PORT, ADC_IN_PIN, GPIO_PRIMARY_MODULE_FUNCTION);

    //Initialize the ADC Module
    /*
     * Base Address for the ADC Module
     * Use internal ADC bit as sample/hold signal to start conversion
     * USE MODOSC 5MHZ Digital Oscillator as clock source
     * Use default clock divider of 1
     */
    ADC_init(ADC_BASE,
             ADC_SAMPLEHOLDSOURCE_SC,
             ADC_CLOCKSOURCE_ADCOSC,
             ADC_CLOCKDIVIDER_1);

    ADC_enable(ADC_BASE);

    /*
     * Base Address for the ADC Module
     * Sample/hold for 16 clock cycles
     * Do not enable Multiple Sampling
     */
    ADC_setupSamplingTimer(ADC_BASE,
                           ADC_CYCLEHOLD_16_CYCLES,
                           ADC_MULTIPLESAMPLESDISABLE);

    //Configure Memory Buffer
    /*
     * Base Address for the ADC Module
     * Use input ADC_IN_CHANNEL
     * Use positive reference of AVcc
     * Use negative reference of AVss
     */
    ADC_configureMemory(ADC_BASE,
                        ADC_IN_CHANNEL,
                        ADC_VREFPOS_AVCC,
                        ADC_VREFNEG_AVSS);

    ADC_clearInterrupt(ADC_BASE,
                       ADC_COMPLETED_INTERRUPT);

    //Enable Memory Buffer interrupt
    ADC_enableInterrupt(ADC_BASE,
                        ADC_COMPLETED_INTERRUPT);
}

//ADC interrupt service routine
#pragma vector=ADC_VECTOR
__interrupt
void ADC_ISR(void)
{
    uint8_t ADCStatus = ADC_getInterruptStatus(ADC_BASE, ADC_COMPLETED_INTERRUPT_FLAG);

    ADC_clearInterrupt(ADC_BASE, ADCStatus);

    if (ADCStatus)
    {
        ADCState = 0; //Not busy anymore
        ADCResult = ADC_getResults(ADC_BASE);
    }
}
